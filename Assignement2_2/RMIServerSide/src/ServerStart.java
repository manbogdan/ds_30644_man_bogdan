import java.rmi.*;
import java.rmi.registry.LocateRegistry;

public class ServerStart {
	   public static void main (String[] argv) {
		   try {
			   System.setSecurityManager(new RMISecurityManager());
			   LocateRegistry.createRegistry(2020);
			   CarService Hello = new CarService();			   		   
			   Naming.rebind("rmi://localhost/ABC", Hello);
 
			   System.out.println("Server is ready.");
			   }catch (Exception e) {
		   			e.printStackTrace();
				   System.out.println("Server failed: " + e);
				}
		   }
}
