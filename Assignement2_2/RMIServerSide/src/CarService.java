import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CarService extends UnicastRemoteObject
         implements ICarService {
	protected CarService() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1492150925330436086L;

	@Override
	public double computeTax(Car car) throws RemoteException {
		// Dummy formula
				if (car.getEngineSize() <= 0) {
					throw new IllegalArgumentException("Engine size must be positive.");
				}
				int sum = 8;
				double tax=0;
				if(car.getEngineSize() > 1601) sum = 18;
				if(car.getEngineSize() > 2001) sum = 72;
				if(car.getEngineSize() > 2601) sum = 144;
				if(car.getEngineSize() > 3001) sum = 290;
				tax=car.getEngineSize()*200/sum;
				return tax;
	}

	@Override
	public double computeSellingPrice(Car car) throws RemoteException {
		if (car.getPurchasingPrice()<0)
			throw new IllegalArgumentException("Purchasing price must be positive.");
		if (car.getYear()<0 || car.getYear()>2018)
			throw new IllegalArgumentException("Year is not correct[0,2018].");
		return car.getPurchasingPrice()-(car.getPurchasingPrice()*(2018-car.getYear())/7);
	}
 }