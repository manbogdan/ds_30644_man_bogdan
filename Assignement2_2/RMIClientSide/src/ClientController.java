import java.awt.event.*;
import java.rmi.*;

import javax.swing.JOptionPane;

public class ClientController {
	private static final String ERROR_MESSAGE =
			"An exception occured while trying to send data to server. Please consult logs for more info!";

	private ClientView clientView;

	public ClientController() {
		clientView = new ClientView();
		clientView.setVisible(true);
		clientView.addBtnTaxActionListener(new TaxActionListener());
		clientView.addBtnPriceActionListener(new PriceActionListener());
	}

	public void printCar(Car car) {
		if (car != null) {
			clientView.printCar(car);
		}
	}
	public void printResult(double res){
		clientView.printResult(res);
	}

	public void displayErrorMessage(String message) {
		clientView.clear();
		JOptionPane.showMessageDialog(clientView, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void displayInfoMessage(String message) {
		clientView.clear();
		JOptionPane.showMessageDialog(clientView, message, "Success", JOptionPane.PLAIN_MESSAGE);
	}

	class TaxActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int year = clientView.getYear();
			int engine = clientView.getEngineSize();
			double price = clientView.getPurchasingPrice();

			ICarService hello;
			try {
	  		        System.setSecurityManager(new RMISecurityManager());
				hello = (ICarService)Naming.lookup("rmi://localhost/ABC");
				double result=hello.computeTax(new Car(year,engine,price));
				System.out.println("Result tax :"+result);
	            printResult(result);
				}catch (Exception ex) {
					ex.printStackTrace();
					System.out.println("Client exception: " + ex);
					displayErrorMessage("Client exception: " + ex);
					}
			}
		}

	class PriceActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int year = clientView.getYear();
			int engine = clientView.getEngineSize();
			double price = clientView.getPurchasingPrice();

			ICarService hello;
			try {
	  		        System.setSecurityManager(new RMISecurityManager());
				hello = (ICarService)Naming.lookup("rmi://localhost/ABC");
				double result=hello.computeSellingPrice(new Car(year,engine,price));
				System.out.println("Result selling price :"+result);
	            printResult(result);
				}catch (Exception ex) {
					System.out.println("Client exception: " + ex);
					displayErrorMessage("Client exception: " + ex);
					}
		}
	}
}