import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ClientView extends JFrame {

	private JPanel contentPane;
	private JTextField year;
	private JTextField engineSize;
	private JTextField purchasingPrice;
	private JButton btnComputeTax;
	private JButton btnComputeSellingPrice;
	private JTextArea textArea;

	public ClientView() {
		setTitle("Car Service");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertCar = new JLabel("Insert car details");
		lblInsertCar.setBounds(10, 11, 120, 14);
		contentPane.add(lblInsertCar);

		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(10, 36, 60, 14);
		contentPane.add(lblYear);

		JLabel lblEngine = new JLabel("Engin size");
		lblEngine.setBounds(10, 61, 60, 14);
		contentPane.add(lblEngine);

		JLabel lblPrice = new JLabel("Purchasing price");
		lblPrice.setBounds(10, 86, 46, 14);
		contentPane.add(lblPrice);

		year = new JTextField();
		year.setBounds(80, 33, 86, 20);
		contentPane.add(year);
		year.setColumns(10);

		engineSize = new JTextField();
		engineSize.setBounds(80, 58, 86, 20);
		contentPane.add(engineSize);
		engineSize.setColumns(10);

		purchasingPrice = new JTextField();
		purchasingPrice.setBounds(80, 83, 86, 20);
		contentPane.add(purchasingPrice);
		purchasingPrice.setColumns(10);

		btnComputeTax = new JButton("Tax");
		btnComputeTax.setBounds(10, 132, 89, 23);
		contentPane.add(btnComputeTax);


		btnComputeSellingPrice = new JButton("Price");
		btnComputeSellingPrice.setBounds(235, 77, 89, 23);
		contentPane.add(btnComputeSellingPrice);
		
		textArea = new JTextArea();
		textArea.setBounds(235, 131, 171, 120);
		contentPane.add(textArea);
	}

	public void addBtnTaxActionListener(ActionListener e) {
		btnComputeTax.addActionListener(e);
	}

	public void addBtnPriceActionListener(ActionListener e) {
		btnComputeSellingPrice.addActionListener(e);
	}
	public int getYear() {
		return Integer.parseInt(year.getText());
	}

	public int getEngineSize() {
		return Integer.parseInt(engineSize.getText());
	}

	public double getPurchasingPrice() {
		return Double.parseDouble(purchasingPrice.getText());
	}
	
	public void printCar(Car car) {
		textArea.setText(car.toString());
	}
	public void printResult(double result) {
		textArea.setText(result+"");
	}

	public void clear() {
		year.setText("");
		engineSize.setText("");
		purchasingPrice.setText("");
	}
}
