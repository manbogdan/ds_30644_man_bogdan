import java.io.Serializable;

public class Car implements Serializable{
    private int year;
    private int engineSize;
    private double purchasingPrice;

    public Car(int year, int engineSize, double purchasingPrice) {
        this.year = year;
        this.engineSize = engineSize;
        this.purchasingPrice = purchasingPrice;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(int engineSize) {
        this.engineSize = engineSize;
    }

    public double getPurchasingPrice() {
        return purchasingPrice;
    }

    public void setPurchasingPrice(double purchasingPrice) {
        this.purchasingPrice = purchasingPrice;
    }
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + engineSize;
		result = prime * result + year;
		result=prime*result+(int)purchasingPrice;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (engineSize != other.engineSize)
			return false;
		if (year != other.year)
			return false;
		if (purchasingPrice!=other.purchasingPrice)
			return false;
		return true;
	}
    @Override
    public String toString() {
        return "Car [year=" + year + ", engineCapacity=" + engineSize + ", pricePurchasing="+purchasingPrice+"]";
    }
}