import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICarService extends Remote{
    double computeTax(Car c) throws RemoteException;
    double computeSellingPrice(Car c) throws RemoteException;

}
