package example.controller;

import com.client.PackageWs;
import com.client.PackageWsService;
import com.client.User;
import example.view.LoginView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Chiti on 12/11/2017.
 */
public class LoginController {

private LoginView loginView;

    public LoginController() {
        loginView = new LoginView();
        loginView.setVisible(true);
        loginView.addBtnLoginListener(new LoginListener());
        loginView.addBtnRegisterActionListener(new RegisterListener());
    }


    public void displayErrorMessage(String message) {
        loginView.clear();
        JOptionPane.showMessageDialog(loginView, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void displayInfoMessage(String message) {
        loginView.clear();
        JOptionPane.showMessageDialog(loginView, message, "Success", JOptionPane.PLAIN_MESSAGE);
    }

/**
 * Provides functionality for the Login button.
 */
class LoginListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        String username=loginView.getUsernameLogin();
        String password=loginView.getPasswordLogin();
        com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);

        User user=service.getUser(username);
        if(user==null || !password.equals(user.getPassword()))
            displayErrorMessage("Invalid login");
        else{
            if (user.getRole()==0)
            {
                loginView.setVisible(false);
                new ClientController(user.getId());
            }else
            {
                loginView.setVisible(false);
                new AdminController();
            }
        }
    }
}

/**
 * Provides functionality for the Register button.
 */
class RegisterListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        String username=loginView.getUsernameRegister();
        String password=loginView.getPasswordRegister();
        com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);

       if( service.saveUser(username,password,(byte)0))
       {
           loginView.setVisible(false);
           User user=service.getUser(username);
           new ClientController(user.getId());
       }
        else displayErrorMessage("Invalid register");
    }
}
}

