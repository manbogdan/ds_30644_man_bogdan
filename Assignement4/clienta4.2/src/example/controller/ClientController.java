package example.controller;

import com.client.Package;
import com.client.PackageWs;
import com.client.PackageWsService;
import com.client.Route;
import example.view.ClientView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by Chiti on 12/11/2017.
 */
public class ClientController {
    private ClientView clientView;
    private static int clientId;
    public ClientController(int id){
        clientId=id;
        clientView=new ClientView();
        clientView.setVisible(true);
        clientView.addBtnFindAllListener(new FindAllListener());
        clientView.addBtnSearchActionListener(new SearchListener());
        clientView.addBtnStatusActionListener(new StatusListener());
    }
    public void displayErrorMessage(String message) {
        clientView.clear();
        JOptionPane.showMessageDialog(clientView, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void displayInfoMessage(String message) {
        clientView.clear();
        JOptionPane.showMessageDialog(clientView, message, "Success", JOptionPane.PLAIN_MESSAGE);
    }
    /**
     * Provides functionality for the find all button.
     */
    class FindAllListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
            List<Package> response = null;
            try {
                response=service.findClientPackages(clientId);
                if (response==null)
                    displayInfoMessage("Client has no packages!");
                else {
                    printPackges(response);
                    //displayInfoMessage("!");
                }
            }
            catch (Exception err1){
                err1.printStackTrace();
                displayErrorMessage("Invalid data!Package doesn't exist!");
            }
        }
    }
    /**
     * Provides functionality for the search button.
     */
    class SearchListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String name=clientView.getIdPackageSearch();
            com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
            List<Package> response = null;
            boolean exists=false;
            try {
                response = service.findClientPackages(clientId);
                if (response == null)
                { clientView.clear();
                    displayInfoMessage("Client has no packages!");}
                else {
                    for(Package p:response)
                        if(p.getName().equals(name))
                        {
                            clientView.print(p.toString());
                            exists=true;
                        }
                    if(!exists)
                        clientView.clear();
                        clientView.print("No such package!");
                }
            }
            catch (Exception err1){
                err1.printStackTrace();
                displayErrorMessage("Invalid data!Package doesn't exist!");
            }
        }
    }
    /**
     * Provides functionality for the status button.
     */
    class StatusListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id=clientView.getIdPackageStatus();
            com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
            List<Package> response = null;
            boolean exists=false;
            try {
                response=service.findClientPackages(clientId);
                if (response==null)
                    displayInfoMessage("Client has no packages!");
                else {
                  for(Package p:response)
                      if(p.getId()==id)
                          exists=true;
                  if(exists)
                  {
                      List<Route> routes=service.findRoutePackage(id);
                      if(routes!=null && routes.size()!=0){
                          printRoutes(routes);
                      }
                      else clientView.print("Package not tracked!");
                  }else{
                      clientView.print("No such package!");
                  }
                }
            }
            catch (Exception err1){
                err1.printStackTrace();
                displayErrorMessage("Invalid data!Package doesn't exist!");
            }
        }
    }
    private void printRoutes(List<Route> routes){
        StringBuilder sb=new StringBuilder();
        sb.append("Route of package:\n");
        for(Route r:routes)
            sb.append(r.toString()+"\n");
        clientView.print(sb.toString());
    }
    private void printPackges(List<Package> packages){
        StringBuilder sb=new StringBuilder();
        sb.append("Packages:\n");
        for(Package r:packages)
            sb.append(r.toString()+"\n");
        clientView.print(sb.toString());
    }
}
