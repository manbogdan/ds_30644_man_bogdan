package example.controller;

import com.client.PackageWs;
import com.client.PackageWsService;
import example.view.AdminView;

import javax.swing.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Chiti on 12/11/2017.
 */
public class AdminController {
    private AdminView adminView;

    public AdminController() {
        adminView = new AdminView();
        adminView.setVisible(true);
        adminView.addBtnAddListener(new AddListener());
        adminView.addBtnDeleteActionListener(new DeleteListener());
        adminView.addBtnTrackListener(new TrackListener());
        adminView.addBtnUpdateActionListener(new UpdateListener());
    }

    public void displayErrorMessage(String message) {
        adminView.clear();
        JOptionPane.showMessageDialog(adminView, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void displayInfoMessage(String message) {
        adminView.clear();
        JOptionPane.showMessageDialog(adminView, message, "Success", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Provides functionality for the Add button.
     */
    class AddListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = adminView.getNameField();
            String description = adminView.getDescription();
            String senderCity = adminView.getSenderCity();
            String destCity = adminView.getDestinationCity();
            String sender = adminView.getSender();
            String receiver = adminView.getReceiver();
            com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
            try {
                boolean response = service.addPackage(name, description, senderCity, destCity, sender, receiver);
                if (!response)
                    displayErrorMessage("Invalid data!Package not saved!");
                else {
                    displayInfoMessage("Package saved!");
                }
            }catch(Exception err){
                err.printStackTrace();
                displayErrorMessage("Invalid data!Package not saved!");
            }
        }
    }

    /**
     * Provides functionality for the Delete button.
     */
    class DeleteListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
        int id=adminView.getIdPackageDelete();
        com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
        try{
            boolean response = service.deletePackage(id);
            if (!response)
                displayErrorMessage("Invalid data!Package not deleted!");
            else {
                displayInfoMessage("Package deleted!");
            }
        }catch(Exception err){
            err.printStackTrace();
            displayErrorMessage("Invalid data!Package not deleted!");
        }
        }
    }
    /**
     * Provides functionality for the Track button.
     */
    class TrackListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id=adminView.getIdPackageTrack();
            com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
            try{
            boolean response = service.trackPackage(id);
            if (!response)
                displayErrorMessage("Invalid data!Package not tracked!");
            else {
                displayInfoMessage("Package tracked!");
            }
            }catch(Exception err){
                err.printStackTrace();
                displayErrorMessage("Invalid data!Package not tracked!");
            }
        }
    }
    /**
     * Provides functionality for the Update button.
     */
    class UpdateListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id=adminView.getIdPackageUpdate();
            String city=adminView.getCityPackageUpdate();
            com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
            boolean response = false;
            try {
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(new Date());
                response=service.addCheckPointPackage(id,city, DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
                if (!response)
                    displayErrorMessage("Invalid data!Package status not updated!");
                else {
                    displayInfoMessage("Package status updated!");
                }
            } catch (DatatypeConfigurationException err) {
                err.printStackTrace();
            }catch (Exception err1){
                err1.printStackTrace();
                displayErrorMessage("Invalid data!Package status not updated!");
            }
        }
    }
}

