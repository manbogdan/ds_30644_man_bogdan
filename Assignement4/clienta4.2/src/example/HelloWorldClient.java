package example;

import com.client.PackageWs;
import com.client.PackageWsService;
import example.controller.ClientController;
import example.controller.LoginController;

public class HelloWorldClient {
  public static void main(String[] argv) {
    com.client.PackageWs service = new PackageWsService().getPort(PackageWs.class);
    //invoke business method
    //System.out.print(service.getPackage(1));
    //System.out.print(service.addPackage("LenovoA500","emag","Turda","Iasi","1","2"));
    //register package for tracking add package status
    //System.out.print(service.trackPackage(2));
    /*try {
      GregorianCalendar c = new GregorianCalendar();
      c.setTime(new Date());
      System.out.print(service.addCheckPointPackage(2,"Cluj-Napoca", DatatypeFactory.newInstance().newXMLGregorianCalendar(c)));
    } catch (DatatypeConfigurationException e) {
      e.printStackTrace();
    }*/
    //list all package client list route package
    //System.out.print(service.findClientPackages(2));
    //System.out.print(service.findRoutePackage(2));
    //user register and login
    //System.out.print(service.getUser("client1"));
    //System.out.print(service.saveUser("c1","pass",(byte)0));
    new LoginController();
    //new ClientController(2);
  }
}
