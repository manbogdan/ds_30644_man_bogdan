package example.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * Created by Chiti on 12/11/2017.
 */
public class ClientView extends JFrame {
    private JPanel contentPane;
    private JTextField idPackageSearch;
    private JTextField idPackageStatus;
    private JButton btnFindAllPackage;
    private JButton btnSearchPackage;
    private JButton btnStatusPackage;
    private JTextArea textArea;
    public ClientView(){
        setTitle("Client");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        //find all packages
        btnFindAllPackage = new JButton("All Packages");
        btnFindAllPackage.setBounds(10, 11, 150, 23);
        contentPane.add(btnFindAllPackage);
        //search package
        JLabel lblPackageSearch = new JLabel("Package id");
        lblPackageSearch.setBounds(10, 36, 120, 14);
        contentPane.add(lblPackageSearch);

        idPackageSearch = new JTextField();
        idPackageSearch.setBounds(80, 36, 86, 20);
        contentPane.add(idPackageSearch);
        idPackageSearch.setColumns(10);

        btnSearchPackage = new JButton("Search");
        btnSearchPackage.setBounds(10, 61, 89, 23);
        contentPane.add(btnSearchPackage);
        //check status
        JLabel lblPackageStatus = new JLabel("Package id");
        lblPackageStatus.setBounds(10, 86, 120, 14);
        contentPane.add(lblPackageStatus);

        idPackageStatus = new JTextField();
        idPackageStatus.setBounds(80, 86, 86, 20);
        contentPane.add(idPackageStatus);
        idPackageStatus.setColumns(10);

        btnStatusPackage = new JButton("Status");
        btnStatusPackage.setBounds(10, 111, 89, 23);
        contentPane.add(btnStatusPackage);
        //display results
        textArea = new JTextArea();
        textArea.setBounds(10, 141, 700, 150);
        contentPane.add(textArea);
    }

    public void addBtnFindAllListener(ActionListener e) {
        btnFindAllPackage.addActionListener(e);
    }

    public void addBtnSearchActionListener(ActionListener e) {
        btnSearchPackage.addActionListener(e);
    }
    public void addBtnStatusActionListener(ActionListener e) {
        btnStatusPackage.addActionListener(e);
    }

    public String getIdPackageSearch() {
        return idPackageSearch.getText();
    }

    public int getIdPackageStatus() {
        return Integer.parseInt(idPackageStatus.getText());
    }
    public void print(String message){
        textArea.setText(message);
    }

    public void clear() {
        idPackageSearch.setText("");
        idPackageStatus.setText("");
        textArea.setText("");
    }
}
