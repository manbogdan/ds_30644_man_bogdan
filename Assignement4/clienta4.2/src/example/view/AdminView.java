package example.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * Created by Chiti on 12/11/2017.
 */
public class AdminView extends JFrame {
    private JPanel contentPane;
    private JTextField name;
    private JTextField description;
    private JTextField senderCity;
    private JTextField destinationCity;
    private JTextField sender;
    private JTextField receiver;
    private JTextField idPackageDelete;
    private JTextField idPackageTrack;
    private JTextField idPackageUpdate;
    private JTextField cityPackageUpdate;
    private JButton btnAddPackage;
    private JButton btnDeletePackage;
    private JButton btnTrackPackage;
    private JButton btnUpdatePackage;
    private JTextArea textArea;
    public AdminView(){
        setTitle("Admin");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        //add
        JLabel lblName = new JLabel("Name");
        lblName.setBounds(10, 11, 60, 14);
        contentPane.add(lblName);

        JLabel lblDescription = new JLabel("Description");
        lblDescription.setBounds(10, 36, 60, 14);
        contentPane.add(lblDescription);

        JLabel lblSenderCity = new JLabel("Send City");
        lblSenderCity.setBounds(10, 61, 60, 14);
        contentPane.add(lblSenderCity);

        JLabel lblDestinationCity = new JLabel("Dest City");
        lblDestinationCity.setBounds(10, 86, 60, 14);
        contentPane.add(lblDestinationCity);

        JLabel lblSender = new JLabel("Sender Id");
        lblSender.setBounds(10, 111, 60, 14);
        contentPane.add(lblSender);

        JLabel lblReceiver = new JLabel("Receive Id");
        lblReceiver.setBounds(10, 136, 60, 14);
        contentPane.add(lblReceiver);

        name = new JTextField();
        name.setBounds(80, 11, 86, 20);
        contentPane.add(name);
        name.setColumns(10);

        description = new JTextField();
        description.setBounds(80, 36, 86, 20);
        contentPane.add(description);
        description.setColumns(10);

        senderCity = new JTextField();
        senderCity.setBounds(80, 61, 86, 20);
        contentPane.add(senderCity);
        senderCity.setColumns(10);

        destinationCity = new JTextField();
        destinationCity.setBounds(80, 86, 86, 20);
        contentPane.add(destinationCity);
        destinationCity.setColumns(10);

        sender = new JTextField();
        sender.setBounds(80, 111, 86, 20);
        contentPane.add(sender);
        sender.setColumns(10);

        receiver = new JTextField();
        receiver.setBounds(80, 136, 86, 20);
        contentPane.add(receiver);
        receiver.setColumns(10);

        btnAddPackage = new JButton("Add");
        btnAddPackage.setBounds(10, 161, 89, 23);
        contentPane.add(btnAddPackage);

        //delete
        JLabel lblIdDelete = new JLabel("Delete id");
        lblIdDelete.setBounds(200, 11, 100, 14);
        contentPane.add(lblIdDelete);

        idPackageDelete = new JTextField();
        idPackageDelete.setBounds(300, 11, 86, 20);
        contentPane.add(idPackageDelete);
        idPackageDelete.setColumns(10);

        btnDeletePackage = new JButton("Delete");
        btnDeletePackage.setBounds(200, 36, 89, 23);
        contentPane.add(btnDeletePackage);
        //track package
        JLabel lblIdTrack = new JLabel("Track id");
        lblIdTrack.setBounds(200, 61, 100, 14);
        contentPane.add(lblIdTrack);

        idPackageTrack = new JTextField();
        idPackageTrack.setBounds(300, 61, 86, 20);
        contentPane.add(idPackageTrack);
        idPackageTrack.setColumns(10);

        btnTrackPackage = new JButton("Track");
        btnTrackPackage.setBounds(200, 86, 89, 23);
        contentPane.add(btnTrackPackage);
        //update status
        JLabel lblIdUpdate = new JLabel("Update id");
        lblIdUpdate.setBounds(200, 111, 100, 14);
        contentPane.add(lblIdUpdate);
        JLabel lblCityUpdate = new JLabel("City update");
        lblCityUpdate.setBounds(200, 136, 100, 14);
        contentPane.add(lblCityUpdate);

        idPackageUpdate = new JTextField();
        idPackageUpdate.setBounds(300, 111, 86, 20);
        contentPane.add(idPackageUpdate);
        idPackageUpdate.setColumns(10);

        cityPackageUpdate = new JTextField();
        cityPackageUpdate.setBounds(300, 136, 86, 20);
        contentPane.add(cityPackageUpdate);
        cityPackageUpdate.setColumns(10);

        btnUpdatePackage = new JButton("Update status");
        btnUpdatePackage.setBounds(200, 161, 89, 23);
        contentPane.add(btnUpdatePackage);

    }
    public void addBtnAddListener(ActionListener e) {
        btnAddPackage.addActionListener(e);
    }

    public void addBtnDeleteActionListener(ActionListener e) {
        btnDeletePackage.addActionListener(e);
    }
    public void addBtnTrackListener(ActionListener e) {
        btnTrackPackage.addActionListener(e);
    }

    public void addBtnUpdateActionListener(ActionListener e) {
        btnUpdatePackage.addActionListener(e);
    }

    public String getNameField() {
        return name.getText();
    }

    public String getDescription() {
        return description.getText();
    }

    public String getSenderCity() {
        return senderCity.getText();
    }

    public String getDestinationCity() {
        return destinationCity.getText();
    }

    public String getSender() {
        return sender.getText();
    }

    public String getReceiver() {
        return receiver.getText();
    }

    public int getIdPackageDelete() {
        return Integer.parseInt(idPackageDelete.getText());
    }

    public int getIdPackageTrack() {
        return Integer.parseInt(idPackageTrack.getText());
    }

    public int getIdPackageUpdate() {
        return Integer.parseInt(idPackageUpdate.getText());
    }

    public String getCityPackageUpdate() {
        return cityPackageUpdate.getText();
    }

    public void print(String massage) {
        textArea.setText(massage);
    }
    public void clear(){
        name.setText("");
        description.setText("");
        senderCity.setText("");
        destinationCity.setText("");
        sender.setText("");
        receiver.setText("");
        idPackageDelete.setText("");
        idPackageTrack.setText("");
        idPackageUpdate.setText("");
       cityPackageUpdate.setText("");
    }
}
