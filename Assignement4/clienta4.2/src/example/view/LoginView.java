package example.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * Created by Chiti on 12/11/2017.
 */
public class LoginView extends JFrame{
    private JPanel contentPane;
    private JTextField usernameLogin;
    private JPasswordField passwordLogin;
    private JTextField usernameRegister;
    private JPasswordField passwordRegister;
    private JButton btnLogin;
    private JButton btnRegister;

    public LoginView() {
        setTitle("Login user");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblUsernameLogin = new JLabel("Username");
        lblUsernameLogin.setBounds(10, 11, 120, 14);
        contentPane.add(lblUsernameLogin);

        JLabel lblPasswordLogin = new JLabel("Password");
        lblPasswordLogin.setBounds(10, 36, 60, 14);
        contentPane.add(lblPasswordLogin);

        JLabel lblUsernameRegister = new JLabel("Register username");
        lblUsernameRegister.setBounds(200, 11, 100, 14);
        contentPane.add(lblUsernameRegister);

        JLabel lblPasswordRegister = new JLabel("Register password");
        lblPasswordRegister.setBounds(200, 36, 100, 14);
        contentPane.add(lblPasswordRegister);

        usernameLogin = new JTextField();
        usernameLogin.setBounds(80, 11, 86, 20);
        contentPane.add(usernameLogin);
        usernameLogin.setColumns(10);

        passwordLogin = new JPasswordField();
        passwordLogin.setBounds(80, 36, 86, 20);
        contentPane.add(passwordLogin);
        passwordLogin.setColumns(10);

        usernameRegister = new JTextField();
        usernameRegister.setBounds(300, 11, 86, 20);
        contentPane.add(usernameRegister);
        usernameRegister.setColumns(10);

        passwordRegister = new JPasswordField();
        passwordRegister.setBounds(300, 36, 86, 20);
        contentPane.add(passwordRegister);
        passwordRegister.setColumns(10);

        btnLogin = new JButton("Login");
        btnLogin.setBounds(10, 60, 89, 23);
        contentPane.add(btnLogin);


        btnRegister = new JButton("Register");
        btnRegister.setBounds(200, 60, 89, 23);
        contentPane.add(btnRegister);

    }

    public void addBtnLoginListener(ActionListener e) {
        btnLogin.addActionListener(e);
    }

    public void addBtnRegisterActionListener(ActionListener e) {
        btnRegister.addActionListener(e);
    }
    public String getUsernameLogin() {
        return usernameLogin.getText();
    }

    public String getPasswordLogin() {
        return passwordLogin.getText();
    }

    public String getUsernameRegister() {
        return usernameRegister.getText();
    }

    public String getPasswordRegister() {
        return passwordRegister.getText();
    }


    public void clear() {
        usernameLogin.setText("");
        passwordLogin.setText("");
        usernameRegister.setText("");
        passwordRegister.setText("");
    }
}
