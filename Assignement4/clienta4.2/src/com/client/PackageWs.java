
package com.client;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.7-b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "PackageWs", targetNamespace = "http://webService.soap.com/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface PackageWs {


    /**
     * 
     * @param arg0
     * @return
     *     returns com.client.Package
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getPackage", targetNamespace = "http://webService.soap.com/", className = "com.client.GetPackage")
    @ResponseWrapper(localName = "getPackageResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.GetPackageResponse")
    @Action(input = "http://webService.soap.com/PackageWs/getPackageRequest", output = "http://webService.soap.com/PackageWs/getPackageResponse")
    public Package getPackage(
        @WebParam(name = "arg0", targetNamespace = "")
        int arg0);

    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "addCheckPointPackage", targetNamespace = "http://webService.soap.com/", className = "com.client.AddCheckPointPackage")
    @ResponseWrapper(localName = "addCheckPointPackageResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.AddCheckPointPackageResponse")
    @Action(input = "http://webService.soap.com/PackageWs/addCheckPointPackageRequest", output = "http://webService.soap.com/PackageWs/addCheckPointPackageResponse")
    public boolean addCheckPointPackage(
        @WebParam(name = "arg0", targetNamespace = "")
        int arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        XMLGregorianCalendar arg2);

    /**
     * 
     * @param arg3
     * @param arg2
     * @param arg5
     * @param arg4
     * @param arg1
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "addPackage", targetNamespace = "http://webService.soap.com/", className = "com.client.AddPackage")
    @ResponseWrapper(localName = "addPackageResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.AddPackageResponse")
    @Action(input = "http://webService.soap.com/PackageWs/addPackageRequest", output = "http://webService.soap.com/PackageWs/addPackageResponse")
    public boolean addPackage(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        String arg2,
        @WebParam(name = "arg3", targetNamespace = "")
        String arg3,
        @WebParam(name = "arg4", targetNamespace = "")
        String arg4,
        @WebParam(name = "arg5", targetNamespace = "")
        String arg5);

    /**
     * 
     * @return
     *     returns java.util.List<com.client.Package>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAllPackage", targetNamespace = "http://webService.soap.com/", className = "com.client.FindAllPackage")
    @ResponseWrapper(localName = "findAllPackageResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.FindAllPackageResponse")
    @Action(input = "http://webService.soap.com/PackageWs/findAllPackageRequest", output = "http://webService.soap.com/PackageWs/findAllPackageResponse")
    public List<Package> findAllPackage();

    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "saveUser", targetNamespace = "http://webService.soap.com/", className = "com.client.SaveUser")
    @ResponseWrapper(localName = "saveUserResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.SaveUserResponse")
    @Action(input = "http://webService.soap.com/PackageWs/saveUserRequest", output = "http://webService.soap.com/PackageWs/saveUserResponse")
    public boolean saveUser(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        Byte arg2);

    /**
     * 
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "trackPackage", targetNamespace = "http://webService.soap.com/", className = "com.client.TrackPackage")
    @ResponseWrapper(localName = "trackPackageResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.TrackPackageResponse")
    @Action(input = "http://webService.soap.com/PackageWs/trackPackageRequest", output = "http://webService.soap.com/PackageWs/trackPackageResponse")
    public boolean trackPackage(
        @WebParam(name = "arg0", targetNamespace = "")
        int arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns java.util.List<com.client.Route>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findRoutePackage", targetNamespace = "http://webService.soap.com/", className = "com.client.FindRoutePackage")
    @ResponseWrapper(localName = "findRoutePackageResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.FindRoutePackageResponse")
    @Action(input = "http://webService.soap.com/PackageWs/findRoutePackageRequest", output = "http://webService.soap.com/PackageWs/findRoutePackageResponse")
    public List<Route> findRoutePackage(
        @WebParam(name = "arg0", targetNamespace = "")
        int arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "deletePackage", targetNamespace = "http://webService.soap.com/", className = "com.client.DeletePackage")
    @ResponseWrapper(localName = "deletePackageResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.DeletePackageResponse")
    @Action(input = "http://webService.soap.com/PackageWs/deletePackageRequest", output = "http://webService.soap.com/PackageWs/deletePackageResponse")
    public boolean deletePackage(
        @WebParam(name = "arg0", targetNamespace = "")
        int arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns com.client.User
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getUser", targetNamespace = "http://webService.soap.com/", className = "com.client.GetUser")
    @ResponseWrapper(localName = "getUserResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.GetUserResponse")
    @Action(input = "http://webService.soap.com/PackageWs/getUserRequest", output = "http://webService.soap.com/PackageWs/getUserResponse")
    public User getUser(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns java.util.List<com.client.Package>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findClientPackages", targetNamespace = "http://webService.soap.com/", className = "com.client.FindClientPackages")
    @ResponseWrapper(localName = "findClientPackagesResponse", targetNamespace = "http://webService.soap.com/", className = "com.client.FindClientPackagesResponse")
    @Action(input = "http://webService.soap.com/PackageWs/findClientPackagesRequest", output = "http://webService.soap.com/PackageWs/findClientPackagesResponse")
    public List<Package> findClientPackages(
        @WebParam(name = "arg0", targetNamespace = "")
        int arg0);

}
