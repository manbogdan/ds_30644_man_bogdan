
package com.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TrackPackageResponse_QNAME = new QName("http://webService.soap.com/", "trackPackageResponse");
    private final static QName _FindAllPackage_QNAME = new QName("http://webService.soap.com/", "findAllPackage");
    private final static QName _FindRoutePackageResponse_QNAME = new QName("http://webService.soap.com/", "findRoutePackageResponse");
    private final static QName _FindClientPackages_QNAME = new QName("http://webService.soap.com/", "findClientPackages");
    private final static QName _AddCheckPointPackageResponse_QNAME = new QName("http://webService.soap.com/", "addCheckPointPackageResponse");
    private final static QName _AddCheckPointPackage_QNAME = new QName("http://webService.soap.com/", "addCheckPointPackage");
    private final static QName _SaveUserResponse_QNAME = new QName("http://webService.soap.com/", "saveUserResponse");
    private final static QName _FindAllPackageResponse_QNAME = new QName("http://webService.soap.com/", "findAllPackageResponse");
    private final static QName _SaveUser_QNAME = new QName("http://webService.soap.com/", "saveUser");
    private final static QName _GetPackage_QNAME = new QName("http://webService.soap.com/", "getPackage");
    private final static QName _AddPackageResponse_QNAME = new QName("http://webService.soap.com/", "addPackageResponse");
    private final static QName _GetUserResponse_QNAME = new QName("http://webService.soap.com/", "getUserResponse");
    private final static QName _FindRoutePackage_QNAME = new QName("http://webService.soap.com/", "findRoutePackage");
    private final static QName _AddPackage_QNAME = new QName("http://webService.soap.com/", "addPackage");
    private final static QName _GetUser_QNAME = new QName("http://webService.soap.com/", "getUser");
    private final static QName _TrackPackage_QNAME = new QName("http://webService.soap.com/", "trackPackage");
    private final static QName _DeletePackageResponse_QNAME = new QName("http://webService.soap.com/", "deletePackageResponse");
    private final static QName _FindClientPackagesResponse_QNAME = new QName("http://webService.soap.com/", "findClientPackagesResponse");
    private final static QName _DeletePackage_QNAME = new QName("http://webService.soap.com/", "deletePackage");
    private final static QName _GetPackageResponse_QNAME = new QName("http://webService.soap.com/", "getPackageResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddPackageResponse }
     * 
     */
    public AddPackageResponse createAddPackageResponse() {
        return new AddPackageResponse();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link GetPackage }
     * 
     */
    public GetPackage createGetPackage() {
        return new GetPackage();
    }

    /**
     * Create an instance of {@link DeletePackageResponse }
     * 
     */
    public DeletePackageResponse createDeletePackageResponse() {
        return new DeletePackageResponse();
    }

    /**
     * Create an instance of {@link FindRoutePackage }
     * 
     */
    public FindRoutePackage createFindRoutePackage() {
        return new FindRoutePackage();
    }

    /**
     * Create an instance of {@link AddPackage }
     * 
     */
    public AddPackage createAddPackage() {
        return new AddPackage();
    }

    /**
     * Create an instance of {@link GetUser }
     * 
     */
    public GetUser createGetUser() {
        return new GetUser();
    }

    /**
     * Create an instance of {@link TrackPackage }
     * 
     */
    public TrackPackage createTrackPackage() {
        return new TrackPackage();
    }

    /**
     * Create an instance of {@link FindClientPackagesResponse }
     * 
     */
    public FindClientPackagesResponse createFindClientPackagesResponse() {
        return new FindClientPackagesResponse();
    }

    /**
     * Create an instance of {@link DeletePackage }
     * 
     */
    public DeletePackage createDeletePackage() {
        return new DeletePackage();
    }

    /**
     * Create an instance of {@link GetPackageResponse }
     * 
     */
    public GetPackageResponse createGetPackageResponse() {
        return new GetPackageResponse();
    }

    /**
     * Create an instance of {@link TrackPackageResponse }
     * 
     */
    public TrackPackageResponse createTrackPackageResponse() {
        return new TrackPackageResponse();
    }

    /**
     * Create an instance of {@link FindClientPackages }
     * 
     */
    public FindClientPackages createFindClientPackages() {
        return new FindClientPackages();
    }

    /**
     * Create an instance of {@link AddCheckPointPackageResponse }
     * 
     */
    public AddCheckPointPackageResponse createAddCheckPointPackageResponse() {
        return new AddCheckPointPackageResponse();
    }

    /**
     * Create an instance of {@link FindAllPackage }
     * 
     */
    public FindAllPackage createFindAllPackage() {
        return new FindAllPackage();
    }

    /**
     * Create an instance of {@link FindRoutePackageResponse }
     * 
     */
    public FindRoutePackageResponse createFindRoutePackageResponse() {
        return new FindRoutePackageResponse();
    }

    /**
     * Create an instance of {@link FindAllPackageResponse }
     * 
     */
    public FindAllPackageResponse createFindAllPackageResponse() {
        return new FindAllPackageResponse();
    }

    /**
     * Create an instance of {@link AddCheckPointPackage }
     * 
     */
    public AddCheckPointPackage createAddCheckPointPackage() {
        return new AddCheckPointPackage();
    }

    /**
     * Create an instance of {@link SaveUserResponse }
     * 
     */
    public SaveUserResponse createSaveUserResponse() {
        return new SaveUserResponse();
    }

    /**
     * Create an instance of {@link SaveUser }
     * 
     */
    public SaveUser createSaveUser() {
        return new SaveUser();
    }

    /**
     * Create an instance of {@link Date }
     * 
     */
    public Date createDate() {
        return new Date();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrackPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "trackPackageResponse")
    public JAXBElement<TrackPackageResponse> createTrackPackageResponse(TrackPackageResponse value) {
        return new JAXBElement<TrackPackageResponse>(_TrackPackageResponse_QNAME, TrackPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "findAllPackage")
    public JAXBElement<FindAllPackage> createFindAllPackage(FindAllPackage value) {
        return new JAXBElement<FindAllPackage>(_FindAllPackage_QNAME, FindAllPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindRoutePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "findRoutePackageResponse")
    public JAXBElement<FindRoutePackageResponse> createFindRoutePackageResponse(FindRoutePackageResponse value) {
        return new JAXBElement<FindRoutePackageResponse>(_FindRoutePackageResponse_QNAME, FindRoutePackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindClientPackages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "findClientPackages")
    public JAXBElement<FindClientPackages> createFindClientPackages(FindClientPackages value) {
        return new JAXBElement<FindClientPackages>(_FindClientPackages_QNAME, FindClientPackages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCheckPointPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "addCheckPointPackageResponse")
    public JAXBElement<AddCheckPointPackageResponse> createAddCheckPointPackageResponse(AddCheckPointPackageResponse value) {
        return new JAXBElement<AddCheckPointPackageResponse>(_AddCheckPointPackageResponse_QNAME, AddCheckPointPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCheckPointPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "addCheckPointPackage")
    public JAXBElement<AddCheckPointPackage> createAddCheckPointPackage(AddCheckPointPackage value) {
        return new JAXBElement<AddCheckPointPackage>(_AddCheckPointPackage_QNAME, AddCheckPointPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "saveUserResponse")
    public JAXBElement<SaveUserResponse> createSaveUserResponse(SaveUserResponse value) {
        return new JAXBElement<SaveUserResponse>(_SaveUserResponse_QNAME, SaveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "findAllPackageResponse")
    public JAXBElement<FindAllPackageResponse> createFindAllPackageResponse(FindAllPackageResponse value) {
        return new JAXBElement<FindAllPackageResponse>(_FindAllPackageResponse_QNAME, FindAllPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "saveUser")
    public JAXBElement<SaveUser> createSaveUser(SaveUser value) {
        return new JAXBElement<SaveUser>(_SaveUser_QNAME, SaveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "getPackage")
    public JAXBElement<GetPackage> createGetPackage(GetPackage value) {
        return new JAXBElement<GetPackage>(_GetPackage_QNAME, GetPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "addPackageResponse")
    public JAXBElement<AddPackageResponse> createAddPackageResponse(AddPackageResponse value) {
        return new JAXBElement<AddPackageResponse>(_AddPackageResponse_QNAME, AddPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "getUserResponse")
    public JAXBElement<GetUserResponse> createGetUserResponse(GetUserResponse value) {
        return new JAXBElement<GetUserResponse>(_GetUserResponse_QNAME, GetUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindRoutePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "findRoutePackage")
    public JAXBElement<FindRoutePackage> createFindRoutePackage(FindRoutePackage value) {
        return new JAXBElement<FindRoutePackage>(_FindRoutePackage_QNAME, FindRoutePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "addPackage")
    public JAXBElement<AddPackage> createAddPackage(AddPackage value) {
        return new JAXBElement<AddPackage>(_AddPackage_QNAME, AddPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "getUser")
    public JAXBElement<GetUser> createGetUser(GetUser value) {
        return new JAXBElement<GetUser>(_GetUser_QNAME, GetUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrackPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "trackPackage")
    public JAXBElement<TrackPackage> createTrackPackage(TrackPackage value) {
        return new JAXBElement<TrackPackage>(_TrackPackage_QNAME, TrackPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "deletePackageResponse")
    public JAXBElement<DeletePackageResponse> createDeletePackageResponse(DeletePackageResponse value) {
        return new JAXBElement<DeletePackageResponse>(_DeletePackageResponse_QNAME, DeletePackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindClientPackagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "findClientPackagesResponse")
    public JAXBElement<FindClientPackagesResponse> createFindClientPackagesResponse(FindClientPackagesResponse value) {
        return new JAXBElement<FindClientPackagesResponse>(_FindClientPackagesResponse_QNAME, FindClientPackagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "deletePackage")
    public JAXBElement<DeletePackage> createDeletePackage(DeletePackage value) {
        return new JAXBElement<DeletePackage>(_DeletePackage_QNAME, DeletePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.soap.com/", name = "getPackageResponse")
    public JAXBElement<GetPackageResponse> createGetPackageResponse(GetPackageResponse value) {
        return new JAXBElement<GetPackageResponse>(_GetPackageResponse_QNAME, GetPackageResponse.class, null, value);
    }

}
