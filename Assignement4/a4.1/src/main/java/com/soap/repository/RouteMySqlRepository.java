package com.soap.repository;

import com.soap.JDBConnectionWrapper;
import com.soap.entity.Package;
import com.soap.entity.Route;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chiti on 12/8/2017.
 */
public class RouteMySqlRepository {
    private final Connection connection;

    public RouteMySqlRepository() {
        connection= new JDBConnectionWrapper("a4").getConnection();
    }

    public boolean deleteAllWithPackage(int id) {
        try {
            Statement statement = connection.createStatement();
            String sql = "DELETE from routes where a_package_id="+id;
            statement.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean save(Route route){
        try {
            PreparedStatement e = this.connection.prepareStatement("INSERT INTO routes values (null, ?, ?,?,?)");
            e.setString(1, route.getCity());
            e.setByte(2, route.getEndpoint());
            e.setDate(3,route.getTime());
            e.setInt(4,route.getaPackage().getId());
            e.executeUpdate();
            return true;
        } catch (SQLException var6) {
            var6.printStackTrace();
            return false;
        }
    }
    public List<Route> getAllForPackage(Package p){
        List<Route> routes = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "Select * from routes WHERE a_package_id="+p.getId();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                routes.add(getRouteFromResultSet(rs,p));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return routes;
    }
    private Route getRouteFromResultSet(ResultSet rs,Package p) throws SQLException {
        int id=rs.getInt("id");
        int idPackage=rs.getInt("a_package_id");
        return new Route(id,rs.getString("city"),p,rs.getDate("time"),rs.getByte("endpoint"));
    }
}
