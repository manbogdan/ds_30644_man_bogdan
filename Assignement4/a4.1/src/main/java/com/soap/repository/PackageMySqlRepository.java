package com.soap.repository;

import com.soap.JDBConnectionWrapper;
import com.soap.entity.Package;
import com.soap.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chiti on 12/8/2017.
 */
public class PackageMySqlRepository {
    private final Connection connection;
    private UserMySqlRepository userMySqlRepository;
    private RouteMySqlRepository routeMySqlRepository;

    public PackageMySqlRepository() {
        connection= new JDBConnectionWrapper("a4").getConnection();
        userMySqlRepository=new UserMySqlRepository();
        routeMySqlRepository=new RouteMySqlRepository();
    }

    public Package getPackage(int id){
        Package p=new Package();
        try {
            Statement statement = connection.createStatement();
            String sql = "Select * from packages where id=" + id;
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next()) {
                p = new Package(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("sender_city"), rs.getString("destination_city"), rs.getBoolean("tracking"), null, null);
                p.setSender(userMySqlRepository.getUser(rs.getInt("sender_id")));
                p.setReceiver(userMySqlRepository.getUser(rs.getInt("receiver_id")));
                return p;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
   public boolean save(Package aPackage){
       try {
           PreparedStatement e = this.connection.prepareStatement("INSERT INTO packages values (null, ?, ?,?,?,?,?,?)");
           e.setString(1, aPackage.getDescription());
           e.setString(2, aPackage.getDestinationCity());
           e.setString(3,aPackage.getName());
           e.setString(4,aPackage.getSenderCity());
           e.setBoolean(5,aPackage.isTracking());
           e.setInt(6,aPackage.getReceiver().getId());
           e.setInt(7,aPackage.getSender().getId());
           e.executeUpdate();
           return true;
       } catch (SQLException var6) {
           var6.printStackTrace();
           return false;
       }
   }

    public boolean delete(Package p) {
        try {
            Statement statement = connection.createStatement();
            String sql = "DELETE from packages where id="+p.getId();
            routeMySqlRepository.deleteAllWithPackage( p.getId());
            statement.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public List<Package> findAll() {
        List<Package> packages = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "Select * from packages";
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                packages.add(getPackageFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return packages;
    }
    public List<Package> findAllClient(int id){
       List<Package> finalList=new ArrayList<>();
       List<Package> list=findAllClientSender(id);
       for(Package p:list)
           finalList.add(p);
       list=findAllClientReceiver(id);
       for (Package p:list)
           finalList.add(p);
       return finalList;
    }
    private List<Package> findAllClientSender(int  id) {
        List<Package> packages = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "Select * from packages where sender_id="+id;
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                packages.add(getPackageFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return packages;
    }
    private List<Package> findAllClientReceiver(int  id) {
        List<Package> packages = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "Select * from packages where receiver_id="+id;
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                packages.add(getPackageFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return packages;
    }
    private Package getPackageFromResultSet(ResultSet rs) throws SQLException {
        int id=rs.getInt("id");
        int idSender=rs.getInt("sender_id");
        User s=userMySqlRepository.getUser(idSender);
        int idReceiver=rs.getInt("receiver_id");
        User r=userMySqlRepository.getUser(idReceiver);
        return new Package(id,rs.getString("name"),rs.getString("description"),rs.getString("sender_city"),rs.getString("destination_city"),rs.getBoolean("tracking"),s,r);
    }
    public boolean updateTracking(int id){
        try {
            byte value=1;
            String sql = "Update packages Set tracking="+value+" where id=" + id;
            PreparedStatement statement = connection
                    .prepareStatement(sql);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
           return false;
        }
    }
}
