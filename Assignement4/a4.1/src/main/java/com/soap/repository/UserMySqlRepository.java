package com.soap.repository;

import com.soap.JDBConnectionWrapper;
import com.soap.entity.User;

import java.sql.*;

/**
 * Created by Chiti on 12/8/2017.
 */
public class UserMySqlRepository {
    private final Connection connection;

    public UserMySqlRepository() {
        connection= new JDBConnectionWrapper("a4").getConnection();
    }

    public User getUser(int id){
        try {
            Statement statement = connection.createStatement();
            String sql = "Select * from users where id=" + id;
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next())
                return new User(rs.getInt("id"),rs.getString("username"),rs.getString("password"),rs.getByte("role"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
    public User findByUsername(String username1){
        try {
            Statement statement = connection.createStatement();
            String sql = "Select * from users where username='" + username1+"'";
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next())
                return new User(rs.getInt("id"),rs.getString("username"),rs.getString("password"),rs.getByte("role"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
    public boolean save(User user){
        try {
            PreparedStatement e = this.connection.prepareStatement("INSERT INTO users values (null, ?, ?,?)");
            e.setString(1, user.getPassword());
            e.setByte(2, user.getRole());
            e.setString(3, user.getUsername());
            e.executeUpdate();
            return true;
        } catch (SQLException var6) {
            var6.printStackTrace();
            return false;
        }
    }
}
