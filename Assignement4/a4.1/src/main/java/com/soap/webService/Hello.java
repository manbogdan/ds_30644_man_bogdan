package com.soap.webService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService
public class Hello {
    private String message = new String("Hello, ");

    public void Hello() {
    }
    public static void main(String[] argv){
     Object implementor=new Hello();
     String address="http://localhost:8080/HelloService";
        Endpoint.publish(address,implementor);
        System.out.println("Web service started");
    }
    @WebMethod
    public String sayHello(String name) {
        return message + name + ".";
    }
}
