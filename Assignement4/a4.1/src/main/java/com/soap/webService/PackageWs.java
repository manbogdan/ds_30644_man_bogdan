package com.soap.webService;

import com.soap.entity.Package;
import com.soap.entity.Route;
import com.soap.entity.User;
import com.soap.service.PackageService;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by Chiti on 12/8/2017.
 */
@WebService
public class PackageWs extends SpringBeanAutowiringSupport {
    //admin methods
    @WebMethod
    public boolean addPackage(String name,String description,String senderCity,String arrivalCity,String sender,String receiver){
        PackageService service=new PackageService();
        service.addPackage(name,description,senderCity,arrivalCity,sender,receiver);
        return true;
    }
    @WebMethod
    public Package getPackage(int id){
        PackageService service=new PackageService();
        return service.getPackage(id);
    }
    @WebMethod
    public boolean deletePackage(int id){
        PackageService service=new PackageService();
        service.deletePackage(id);
        return true;
    }
    @WebMethod
    public boolean trackPackage(int id){
        PackageService service=new PackageService();
        service.trackPackage(id);
        return true;
    }
    @WebMethod
    public boolean addCheckPointPackage(int id,String city,Date date){
        PackageService service=new PackageService();
        service.addCheckPointPackage(id,city,date);
        return true;
    }

    @WebMethod
    public List<Package> findAllPackage(){
        PackageService service=new PackageService();
        return service.findAllPackage();
    }
    //client methods
    @WebMethod
    public List<Route> findRoutePackage(int id){
        PackageService service=new PackageService();
        return service.findRoutePackage(id);
    }
    @WebMethod
    public List<Package> findClientPackages(int id){
        PackageService service=new PackageService();
        return service.findClientPackage(id);
    }

    //login register
    @WebMethod
    public User getUser(String username){
        PackageService service=new PackageService();
        return service.getUser(username);
    }
    @WebMethod
    public boolean saveUser(String username,String password,Byte role){
        PackageService service=new PackageService();
        return service.saveUser(username,password,role);
    }
}
