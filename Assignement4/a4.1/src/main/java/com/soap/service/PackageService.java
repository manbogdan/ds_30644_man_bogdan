package com.soap.service;

import com.soap.entity.Package;
import com.soap.entity.Route;
import com.soap.entity.User;
import com.soap.repository.PackageMySqlRepository;
import com.soap.repository.RouteMySqlRepository;
import com.soap.repository.UserMySqlRepository;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Chiti on 12/8/2017.
 */
@Service
public class PackageService {

    private PackageMySqlRepository packageMySqlRepository;
    private UserMySqlRepository userMySqlRepository;
    private RouteMySqlRepository routeMySqlRepository;

    public PackageService() {
        packageMySqlRepository= new PackageMySqlRepository();
        userMySqlRepository=new UserMySqlRepository();
        routeMySqlRepository=new RouteMySqlRepository();
    }

    public void addPackage(String name,String description,String senderCity,String arrivalCity,String sender,String receiver){
        User sender1=userMySqlRepository.getUser(Integer.parseInt(sender));
        User receiver1=userMySqlRepository.getUser(Integer.parseInt(receiver));
        packageMySqlRepository.save(new Package(name,description,senderCity,arrivalCity,false,sender1,receiver1));
    }

    public Package getPackage(int id){
        return packageMySqlRepository.getPackage(id);
    }

    public boolean deletePackage(int id){
        Package p=packageMySqlRepository.getPackage(id);
        packageMySqlRepository.delete(p);
        return true;
    }

    public boolean trackPackage(int id){
        Package p=packageMySqlRepository.getPackage(id);
        if (p.isTracking())
            return false;
        p.setTracking(true);
        //packageMySqlRepository.save(p);
        packageMySqlRepository.updateTracking(id);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        routeMySqlRepository.save(new Route(p.getSenderCity(),p,java.sql.Date.valueOf( dateFormat.format(date) ),(byte)1));
        Calendar c = Calendar.getInstance(); // starts with today's date and time
        c.add(Calendar.DAY_OF_YEAR, 2);  // advances day by 2
        Date dateDestination = c.getTime();
        System.out.println(dateFormat.format(dateDestination));
        routeMySqlRepository.save(new Route(p.getDestinationCity(),p,java.sql.Date.valueOf( dateFormat.format(dateDestination) ),(byte)1));
        return true;
    }

    public boolean addCheckPointPackage(int id,String city,Date date){
        Package p=packageMySqlRepository.getPackage(id);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //System.out.println("in service:"+dateFormat.format(date));
        if (!p.isTracking())
            return false;
        routeMySqlRepository.save(new Route(city,p,java.sql.Date.valueOf( dateFormat.format(date) ),(byte)0));
        return true;
    }


    public List<Package> findAllPackage(){
        return packageMySqlRepository.findAll();
    }

    public List<Route> findRoutePackage(int id){
        Package p=packageMySqlRepository.getPackage(id);
        return routeMySqlRepository.getAllForPackage(p);
    }

    public List<Package> findClientPackage(int id) {
        return packageMySqlRepository.findAllClient(id);
    }

    public User getUser(String username) {
        return userMySqlRepository.findByUsername(username);
    }

    public boolean saveUser(String username, String password, Byte role) {
        return userMySqlRepository.save(new User(username,password,role));
    }
}
