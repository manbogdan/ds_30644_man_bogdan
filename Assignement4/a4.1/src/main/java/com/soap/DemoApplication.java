package com.soap;

import com.soap.webService.PackageWs;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.ws.Endpoint;

@SpringBootApplication
public class DemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		Object implementor=new PackageWs();
		String address="http://localhost:9000/PackageService";
		Endpoint.publish(address,implementor);
		System.out.println("Web service started");
	}
}
