package com.soap.entity;
import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Chiti on 12/8/2017.
 */
@Entity
@Table(name = "packages")
public class Package implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "sender_city")
    private String senderCity;

    @Column(name = "destination_city")
    private String destinationCity;

    @Column(name = "tracking")
    private boolean tracking;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private User sender;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private User receiver;

    public  Package(){}

    public Package(String name, String description, String senderCity, String destinationCity, boolean tracking, User sender, User receiver) {
        this.name = name;
        this.description = description;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
        this.sender = sender;
        this.receiver = receiver;
    }
    public Package(int id,String name, String description, String senderCity, String destinationCity, boolean tracking, User sender, User receiver) {
        this.name = name;
        this.description = description;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
        this.sender = sender;
        this.receiver = receiver;
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCity='" + senderCity + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", tracking=" + tracking +
                ", sender=" + sender +
                ", receiver=" + receiver +
                '}';
    }
}