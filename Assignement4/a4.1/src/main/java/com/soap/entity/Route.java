package com.soap.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Chiti on 12/8/2017.
 */
@Entity
@Table(name = "routes")
public class Route implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "city")
    private String city;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Package aPackage;

    @Column(name="time")
    private Date time;

    @Column(name = "endpoint")//1-endpoint,0-checkpoint
    private Byte endpoint;
    public Route(){}

    public Route(String city, Package aPackage, Date time, Byte endpoint) {
        this.city = city;
        this.aPackage = aPackage;
        this.time = time;
        this.endpoint = endpoint;
    }
    public Route(int id,String city, Package aPackage, Date time, Byte endpoint) {
        this.city = city;
        this.aPackage = aPackage;
        this.time = time;
        this.endpoint = endpoint;
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Byte getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Byte endpoint) {
        this.endpoint = endpoint;
    }
}
