package com.soap.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Chiti on 12/8/2017.
 */
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "username", unique = true)
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "role")//1-admin,0-client
    private Byte role;

    public User() {
    }

    public User(String username, String password, Byte role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }
    public User(int id,String username, String password, Byte role) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Byte getRole() {
        return role;
    }

    public void setRole(Byte role) {
        this.role = role;
    }
}