package dao;

import entities.User;
import org.hibernate.*;
import java.util.List;

public class UsersDAO {
    private SessionFactory factory;

    public UsersDAO(SessionFactory factory) {
        this.factory = factory;
    }
    @SuppressWarnings("unchecked")
    public User findByUsernameAndPassword(String username, String password) {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<User> user = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            user = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        if (user!=null && user.get(0).getPassword().equals(password))
        return user.get(0);
        else return null;
    }
}
