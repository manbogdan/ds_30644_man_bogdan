package dao;

import dto.FlightDTO;
import entities.Cities;
import entities.Flights;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class FlightDAO {
    private SessionFactory factory;
    private CityDAO cityDAO;
    public FlightDAO(SessionFactory factory) {
        this.factory = factory;
        cityDAO =new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    @SuppressWarnings("unchecked")
    public Flights addFlight(FlightDTO flight) {
        int flightId = -1;
        Session session = factory.openSession();
        //Cities arrivalCity= cityDAO.findCity(flight.idArrivalCity);
        //Cities departureCity= cityDAO.findCity(flight.idDepartureCity);
        Flights flightSave=new Flights();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            flightId = (Integer) session.save(flightSave);
            flightSave.setId(flightId);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return flightSave;
    }

    @SuppressWarnings("unchecked")
    public Flights deleteFlight(int flightNumber) {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Flights> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Flights WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            if (!flights.isEmpty())
            {
                for (Flights flight : flights) {
                    Hibernate.initialize(flight.getCitiesByArrivalCityId());
                    Hibernate.initialize(flight.getCitiesByDepartureCityId());
                }
                session.delete(flights.get(0));
            }
            else System.out.println("No such flight");
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public Flights findFlight(int flightNumber) {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Flights> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Flights WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            for (Flights flight : flights) {
                Hibernate.initialize(flight.getCitiesByArrivalCityId());
                Hibernate.initialize(flight.getCitiesByDepartureCityId());
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public List<Flights> findFlights() {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Flights> flights = null;
        try {
            transaction = session.beginTransaction();
            flights = session.createQuery("FROM Flights ").list();
            for (Flights flight : flights) {
                Hibernate.initialize(flight.getCitiesByArrivalCityId());
                Hibernate.initialize(flight.getCitiesByDepartureCityId());
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return flights;
    }

    public List<Flights> findFlightCity(int idDeparture,int  idArrival) {
        Session session = factory.openSession();
        Transaction transaction = null;
        Cities departureCity= cityDAO.findCity(idDeparture);
        List<Flights> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Flights WHERE citiesByDepartureCityId = :departureCity");
            query.setParameter("departureCity", departureCity);
            flights = query.list();
            for (Flights flight : flights) {
                Hibernate.initialize(flight.getCitiesByArrivalCityId());
                Hibernate.initialize(flight.getCitiesByDepartureCityId());
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        List<Flights> flightsReturn = new ArrayList<>();
        if (flights!=null)
        {
            for(Flights flight:flights) {
                if (flight.getCitiesByArrivalCityId().getId()==idArrival) flightsReturn.add(flight);
            }
        }
        return flightsReturn.size()==0? null: flightsReturn ;
    }
    public Flights updateFlights(int flightNumber, String column, String value){
        Session session = factory.openSession();
        Transaction transaction = null;
        Flights flightSave=findFlight(flightNumber);
        try {
            transaction = session.beginTransaction();
            flightSave.setAirplaneType(value);
            session.saveOrUpdate(flightSave);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return flightSave;
    }
    public Flights updateFlights(int flightNumber, String column, int value){
        Session session = factory.openSession();
        Transaction ttransaction = null;
        Flights flightSave=findFlight(flightNumber);
        try {
            ttransaction = session.beginTransaction();
            switch(column){
                case "DepartureCity":
                    flightSave.setCitiesByDepartureCityId(cityDAO.findCity(value));
                    break;
                case "ArrivalCity":
                    flightSave.setCitiesByArrivalCityId(cityDAO.findCity(value));
                    break;
            }
            session.saveOrUpdate(flightSave);
            ttransaction.commit();
        } catch (HibernateException e) {
            if (ttransaction != null) {
                ttransaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return flightSave;
    }
    public Flights updateFlights(int flightNumber, String column, Timestamp value){
        Session session = factory.openSession();
        Transaction transaction = null;
        Flights flightSave=findFlight(flightNumber);
        try {
            transaction = session.beginTransaction();
            switch(column){
                case "DepartureTime":
                    flightSave.setDepartureCityDatetime(value);
                    break;
                case "ArrivalTime":
                    flightSave.setArrivalCityDatetime(value);
                    break;
            }
            session.saveOrUpdate(flightSave);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return flightSave;
    }
}
