package dao;

import dto.CityDTO;
import entities.Cities;
import org.hibernate.*;
import java.util.List;

public class CityDAO {
    private SessionFactory factory;

    public CityDAO(SessionFactory factory) {
        this.factory = factory;
    }

    @SuppressWarnings("unchecked")
    public Cities addCity(CityDTO city) {
        int cityId = -1;
        Session session = factory.openSession();
        Cities citySave=new Cities();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            cityId = (Integer) session.save(citySave);
            citySave.setId(cityId);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e.toString());
        } finally {
            session.close();
        }
        return citySave;
    }

    @SuppressWarnings("unchecked")
    public Cities deleteCity(String name) {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Cities> cities = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Cities WHERE name = :name");
            query.setParameter("name", name);
            cities = query.list();
            if (!cities.isEmpty())
                session.delete(cities.get(0));
            else System.out.println("No such city");
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }
    @SuppressWarnings("unchecked")
    public List<Cities> findCities() {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Cities> cities = null;
        try {
            transaction = session.beginTransaction();
            cities = session.createQuery("FROM Cities").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return cities;
    }
    @SuppressWarnings("unchecked")
    public Cities findCity(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Cities> cities = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Cities WHERE id = :id");
            query.setParameter("id", id);
            cities = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }
}
