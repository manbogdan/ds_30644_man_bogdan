package service;

import dao.FlightDAO;
import dto.FlightDTO;
import entities.Flights;
import org.hibernate.cfg.Configuration;

import java.sql.Timestamp;
import java.util.List;

public class FlightService {
    private FlightDAO flightDAO;
    public FlightService(){
        flightDAO =new FlightDAO(new Configuration().configure().buildSessionFactory());
    }
    public List<Flights> findFlights(){
        return flightDAO.findFlights();
    }
    public Flights addFlights(FlightDTO flight){
        return flightDAO.addFlight(flight);
    }
    public Flights deleteFlights(int flightNumber){
        return flightDAO.deleteFlight(flightNumber);
    }
    public List<Flights> findFlightsCities(int idDeparture,int idArrival){
        return flightDAO.findFlightCity(idDeparture,idArrival);
    }
    public Flights findFlight(int flightNumber){
        return flightDAO.findFlight(flightNumber);
    }
    public Flights updateFlights(int flightNumber, String column, Timestamp date) {
        return flightDAO.updateFlights(flightNumber,column,date);
    }
    public Flights updateFlights(int flightNumber, String column, int  idCity) {
        return flightDAO.updateFlights(flightNumber,column,idCity);
    }
    public Flights updateFlights(int flightNumber, String column, String  value) {
        return flightDAO.updateFlights(flightNumber,column,value);
    }
}
