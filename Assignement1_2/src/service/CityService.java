package service;

import dao.CityDAO;
import dto.CityDTO;
import entities.Cities;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class CityService {
    private CityDAO cityDAO;
    public CityService(){
        cityDAO =new CityDAO(new Configuration().configure().buildSessionFactory());
    }
    public List<Cities> findCities(){
        return cityDAO.findCities();
    }
    public Cities addCities(CityDTO city){
        return cityDAO.addCity(city);
    }
    public Cities deleteCities(String name){
        return cityDAO.deleteCity(name);
    }
}
