package service;

import dao.UsersDAO;
import entities.User;
import org.hibernate.cfg.Configuration;

public class UsersService {
    private UsersDAO usersDAO;
    public UsersService(){

        usersDAO =new UsersDAO(new Configuration().configure().buildSessionFactory());
    }

    public User findByUsernameAndPassword(String username, String password) {
        return usersDAO.findByUsernameAndPassword(username,password);
    }
}
