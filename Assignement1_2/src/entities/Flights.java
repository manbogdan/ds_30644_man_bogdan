package entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Flights {
    private int id;
    private Integer flightNumber;
    private String airplaneType;
    private Timestamp departureCityDatetime;
    private Timestamp arrivalCityDatetime;
    private Cities citiesByDepartureCityId;
    private Cities citiesByArrivalCityId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "flight_number")
    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Basic
    @Column(name = "airplane_type")
    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    @Basic
    @Column(name = "departure_city_datetime")
    public Timestamp getDepartureCityDatetime() {
        return departureCityDatetime;
    }

    public void setDepartureCityDatetime(Timestamp departureCityDatetime) {
        this.departureCityDatetime = departureCityDatetime;
    }

    @Basic
    @Column(name = "arrival_city_datetime")
    public Timestamp getArrivalCityDatetime() {
        return arrivalCityDatetime;
    }

    public void setArrivalCityDatetime(Timestamp arrivalCityDatetime) {
        this.arrivalCityDatetime = arrivalCityDatetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flights flights = (Flights) o;

        if (id != flights.id) return false;
        if (flightNumber != null ? !flightNumber.equals(flights.flightNumber) : flights.flightNumber != null)
            return false;
        if (airplaneType != null ? !airplaneType.equals(flights.airplaneType) : flights.airplaneType != null)
            return false;
        if (departureCityDatetime != null ? !departureCityDatetime.equals(flights.departureCityDatetime) : flights.departureCityDatetime != null)
            return false;
        if (arrivalCityDatetime != null ? !arrivalCityDatetime.equals(flights.arrivalCityDatetime) : flights.arrivalCityDatetime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (flightNumber != null ? flightNumber.hashCode() : 0);
        result = 31 * result + (airplaneType != null ? airplaneType.hashCode() : 0);
        result = 31 * result + (departureCityDatetime != null ? departureCityDatetime.hashCode() : 0);
        result = 31 * result + (arrivalCityDatetime != null ? arrivalCityDatetime.hashCode() : 0);
        return result;
    }
    @ManyToOne
    @JoinColumn(name = "departure_city_id", referencedColumnName = "id")
    public Cities getCitiesByDepartureCityId() {
        return citiesByDepartureCityId;
    }

    public void setCitiesByDepartureCityId(Cities citiesByDepartureCityId) {
        this.citiesByDepartureCityId = citiesByDepartureCityId;
    }

    @ManyToOne
    @JoinColumn(name = "arrival_city_id", referencedColumnName = "id")
    public Cities getCitiesByArrivalCityId() {
        return citiesByArrivalCityId;
    }

    public void setCitiesByArrivalCityId(Cities citiesByArrivalCityId) {
        this.citiesByArrivalCityId = citiesByArrivalCityId;
    }
    @Override
    public String toString(){
        return "Flight number:"+getFlightNumber()+" departure:"+getCitiesByDepartureCityId().getName()+" at time:"+getArrivalCityDatetime().toString()+" arrives to:"+getCitiesByArrivalCityId().getName()+ " at time:"+getArrivalCityDatetime().toString();
    }
}
