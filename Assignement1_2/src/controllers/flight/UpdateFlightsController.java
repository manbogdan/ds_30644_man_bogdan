package controllers.flight;

import entities.Flights;
import service.FlightService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/UpdateFlights")
public class UpdateFlightsController extends HttpServlet {
    public FlightService flightService =new FlightService();
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws  IOException
    {
        String column=request.getParameter("updateFlightColumn");
        String airplaneType=request.getParameter("updateAirplaneType");
        int flightNumber= Integer.parseInt(request.getParameter("updateFlightNumber"));
        int idArrivalCity= Integer.parseInt(request.getParameter("updateArrivalCity"));
        int idDepartureCity= Integer.parseInt(request.getParameter("updateDepartureCity"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        Date parsedDateArrival=new Date();
        Date parsedDateDeparture=new Date();
        Flights flightResponse=new Flights();
        switch(column){
            case "AirplaneType":
                flightResponse= flightService.updateFlights(flightNumber,column,airplaneType);
                break;
            case "DepartureCity":
                flightResponse= flightService.updateFlights(flightNumber,column,idDepartureCity);
                break;
            case "DepartureTime":
                try{
                    parsedDateDeparture = dateFormat.parse(request.getParameter("updateDepartureDate"));}
                catch (Exception e){
                    System.out.println(e.toString());
                }
                Timestamp departureDate=new java.sql.Timestamp(parsedDateDeparture.getTime());
                flightResponse= flightService.updateFlights(flightNumber,column,departureDate);
                break;
            case "ArrivalCity":
                flightResponse= flightService.updateFlights(flightNumber,column,idArrivalCity);
                break;
            case "ArrivalTime":
                try{
                    parsedDateArrival = dateFormat.parse(request.getParameter("updateArrivalDate"));}
                catch (Exception e){
                    System.out.println(e.toString());
                }
                Timestamp arrivalDate=new java.sql.Timestamp(parsedDateArrival.getTime());
                flightResponse= flightService.updateFlights(flightNumber,column,arrivalDate);
                break;
            default:
                flightResponse.setId(-1);
        }
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (flightResponse.getId()<0)
        {
            out.print("<p>Could not save flight!</p><br>"+"<a href=\"AddFlights\">Back</a>");
        }else
            out.print("<p>Saved the flight"+flightResponse.toString()+"</p>"+"<a href=\"AddFlights\">Back</a>");
    }
}
