package controllers.flight;

import entities.Flights;
import service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/DeleteFlights")
public class DeleteFlightsController extends HttpServlet{
    FlightService flightService =new FlightService();
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException
    {
        Flights flightResponse= flightService.deleteFlights(Integer.parseInt(request.getParameter("flightNumberDelete")));
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (flightResponse==null)
        {
            out.print("<p>Could not delete flight!</p><br>"+"<a href=\"AddFlights\">Back</a>");
        }else
            out.print("<p>Deleted the city"+flightResponse.toString()+"</p>"+"<a href=\"AddFlights\">Back</a>");
    }
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException
    {
        List<Flights> flights= flightService.findFlightsCities(Integer.parseInt(request.getParameter("departureCity2")),Integer.parseInt(request.getParameter("arrivalCity2")));
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (flights==null)
        {
            out.print("<p>No such flight!</p><br>");
            out.print("<a href=\"AddFlights\">Back</a>");
        }else
        {
            for (Flights flight:flights
                ) {
            out.print("<p>"+ flight.toString()+"</p><br>");
        }
            out.print("<a href=\"AddFlights\">Back</a>");}
    }
}
