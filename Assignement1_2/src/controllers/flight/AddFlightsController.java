package controllers.flight;

import dto.CityDTO;
import dto.FlightDTO;
import entities.Cities;
import entities.Flights;
import service.CityService;
import service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet("/AddFlights")
public class AddFlightsController extends HttpServlet {
    private CityService cityService =new CityService();
    private FlightService flightService =new FlightService();
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException
    {
        List<Cities> cities= cityService.findCities();
        List<CityDTO> modelCities=new ArrayList<>();
        for (Cities city: cities) {
            modelCities.add(new CityDTO(city.getId(),city.getLatitude(),city.getLongitude(),city.getName()));
        }
        request.setAttribute("cities", modelCities);
        List<Flights> flights= flightService.findFlights();
        List<FlightDTO> modelFlights=new ArrayList<>();
        for (Flights flight: flights) {
            modelFlights.add(new FlightDTO(flight.getId(),flight.getFlightNumber(),flight.getAirplaneType(),flight.getCitiesByDepartureCityId().getName(),flight.getDepartureCityDatetime().toString(),
                    flight.getCitiesByArrivalCityId().getName(),flight.getArrivalCityDatetime().toString()));
            System.out.println(flight.toString());
        }
        request.setAttribute("flights", modelFlights);
        request.getRequestDispatcher("/AddFlights.jsp").forward(request, response);
    }
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException
    {
        FlightDTO flightDTO=new FlightDTO();
        int flightNumber= Integer.parseInt(request.getParameter("flightNumber"));
        int idArrivalCity= Integer.parseInt(request.getParameter("arrivalCity"));
        int idDepartureCity= Integer.parseInt(request.getParameter("departureCity"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        Date parsedDateArrival=new Date();
        Date parsedDateDeparture=new Date();
        try{
       parsedDateArrival = dateFormat.parse(request.getParameter("arrivalDate"));
       parsedDateDeparture = dateFormat.parse(request.getParameter("departureDate"));}
        catch (Exception e){
          System.out.println(e.toString());
        }
        Timestamp arrivalDate=new java.sql.Timestamp(parsedDateArrival.getTime());
        Timestamp departureDate=new java.sql.Timestamp(parsedDateDeparture.getTime());
        flightDTO.flightNumber=flightNumber;
        //flightDTO.idDepartureCity=idDepartureCity;
        //flightDTO.departureDateTime=departureDate;
        //flightDTO.idArrivalCity=idArrivalCity;
        //flightDTO.arrivalDateTime=arrivalDate;
        flightDTO.airplaneType=request.getParameter("airplaneType");
        Flights flightResponse= flightService.addFlights(flightDTO);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (flightResponse.getId()<0)
        {
            out.print("<p>Could not save flight!</p><br>"+"<a href=\"AddFlights\">Back</a>");
        }else
            out.print("<p>Saved the flight"+flightResponse.toString()+"</p>"+"<a href=\"AddFlights\">Back</a>");
    }
}
