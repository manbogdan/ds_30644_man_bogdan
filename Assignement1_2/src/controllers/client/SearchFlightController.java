package controllers.client;

import dao.FlightDAO;
import entities.Flights;
import org.hibernate.cfg.Configuration;
import service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/SearchFlight")
public class SearchFlightController extends HttpServlet{
    private FlightService flightService =new FlightService();
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws IOException
    {
        FlightDAO flightDAO =new FlightDAO(new Configuration().configure().buildSessionFactory());
        List<Flights> flights= flightService.findFlightsCities(Integer.parseInt(request.getParameter("departureCity2")),Integer.parseInt(request.getParameter("arrivalCity2")));
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (flights==null)
        {
            out.print("<p>No such flight!</p><br>");
            out.print("<a href=\"Client\">Back</a>");
        }else
        {
            for (Flights flight:flights
                    ) {
                out.print("<p>"+ flight.toString()+"</p><br>");
            }
            out.print("<a href=\"Client\">Back</a>");}
    }
}
