package controllers.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import dto.TimeDTO;
import entities.Flights;
import service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;

@WebServlet("/FindTime")
public class FindTimeController extends HttpServlet {
    private static final String apiKey="AIzaSyDlo5ESQjBfBUFTV65gYoeJILLrEHmbiwk";
    private static final String baseUrl="https://maps.googleapis.com/maps/api/timezone/json?location=";
    private FlightService flightService =new FlightService();
    protected void doPost(HttpServletRequest request,

                          HttpServletResponse response) throws ServletException, IOException
    {
        Flights flight= flightService.findFlight(Integer.parseInt(request.getParameter("flightNumberSearchTime")));
        String city=request.getParameter("citySearchTime");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (flight==null)
        {
            out.print("<p>No such flight!</p><br>");
            out.print("<a href=\"Client\">Back</a>");
        }else
        {
            if(city.equals("departure")){
            out.print("Local time in departure city:"+flight.getCitiesByDepartureCityId().getName()+" is:"+flight.getDepartureCityDatetime().toString()+" but in the arrival city:"+flight.getCitiesByArrivalCityId().getName()+" the local time is:"+getTimeInLocation(flight.getCitiesByArrivalCityId().getLatitude(),flight.getCitiesByArrivalCityId().getLongitude(),flight.getDepartureCityDatetime())+"</p><br>");
            out.print("<a href=\"Client\">Back</a>");
            }
            else{
                out.print("Local time in arrival city:"+flight.getCitiesByArrivalCityId().getName()+" will be:"+getTimeInLocation(flight.getCitiesByDepartureCityId().getLatitude(),flight.getCitiesByDepartureCityId().getLongitude(),flight.getArrivalCityDatetime())+" but in the departure city:"+flight.getCitiesByDepartureCityId().getName()+" local time will be:"+flight.getArrivalCityDatetime()+"</p><br>");
                out.print("<a href=\"Client\">Back</a>");
            }
    }
    }

    public static String getTimeInLocation(double latitude,double longitude,Timestamp time)
    {
        String jsonResult=getTimeForCoordinates(latitude,longitude,time);
        String timeInLocation=toEntity(jsonResult,time);
        System.out.println(jsonResult);
        return timeInLocation;
    }

    private static String toEntity(String jsonString,Timestamp timestamp)
    {
        try{
            Gson gson = new GsonBuilder().create();
            TimeDTO timeInfo = gson.fromJson(jsonString, TimeDTO.class);
            System.out.println(""+timestamp.getTime()/1000 + " dstoffset "+timeInfo.dstOffset +" rawOffset "+ timeInfo.rawOffset);
            Timestamp localDate = new Timestamp((timestamp.getTime()/1000 + timeInfo.dstOffset + timeInfo.rawOffset)*1000);
            return localDate.toString();
        }
        catch(JsonSyntaxException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    private static String getTimeForCoordinates(double latitude,double longitude,Timestamp time) throws RuntimeException{

        String googleUrl = baseUrl+"38.908133,-77.047119&timestamp=1458000000&key="+apiKey;

        try {
            googleUrl = baseUrl+ URLEncoder.encode(latitude+"", "utf-8")+","+URLEncoder.encode(longitude+"", "utf-8")+"&timestamp="+URLEncoder.encode(time.getTime()/1000+"", "utf-8")+"&key="+apiKey;
            System.out.println(googleUrl);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
            System.out.println("Error"+e1.toString());
        }

        StringBuilder strBuf = new StringBuilder();

        HttpURLConnection conn=null;
        BufferedReader reader=null;
        try{
            URL url = new URL(googleUrl);
            conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("apikey",apiKey);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + conn.getResponseCode());
            }

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
            String output = null;
            while ((output = reader.readLine()) != null)
                strBuf.append(output);
        }catch(MalformedURLException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        finally
        {
            if(reader!=null)
            {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(conn!=null)
            {
                conn.disconnect();
            }
        }

        return strBuf.toString();
    }
}
