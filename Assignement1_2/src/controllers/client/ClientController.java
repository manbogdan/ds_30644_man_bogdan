package controllers.client;

import dto.CityDTO;
import dto.FlightDTO;
import entities.Cities;
import entities.Flights;
import service.CityService;
import service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/Client")
public class ClientController extends HttpServlet {
    private CityService cityService =new CityService();
    private FlightService flightService =new FlightService();
    protected void doPost(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException
    {
        List<Cities> cities= cityService.findCities();
        List<CityDTO> modelCities=new ArrayList<>();
        for (Cities city: cities) {
            modelCities.add(new CityDTO(city.getId(),city.getLatitude(),city.getLongitude(),city.getName()));
        }
        request.setAttribute("cities", modelCities);
        List<Flights> flights= flightService.findFlights();
        List<FlightDTO> modelFlights=new ArrayList<>();
        for (Flights flight: flights) {
            modelFlights.add(new FlightDTO(flight.getId(),flight.getFlightNumber(),flight.getAirplaneType(),flight.getCitiesByDepartureCityId().getName(),flight.getDepartureCityDatetime().toString(),
                    flight.getCitiesByArrivalCityId().getName(),flight.getArrivalCityDatetime().toString()));
        }
        request.setAttribute("flights", modelFlights);
        request.getRequestDispatcher("/Client.jsp").forward(request, response);
    }
    protected void doGet(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request,response);
    }

}
