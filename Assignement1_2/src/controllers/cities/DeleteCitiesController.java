package controllers.cities;

import entities.Cities;
import service.CityService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/DeleteCities")
public class DeleteCitiesController extends HttpServlet{
    CityService cityService =new CityService();
    protected void doGet(HttpServletRequest request,
                            HttpServletResponse response) throws ServletException, IOException
    {
        String name= request.getParameter("nameCityDelete");
        Cities cityResponse= cityService.deleteCities(name);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (cityResponse==null)
        {
            out.print("<p>Could not delete city!</p><br>"+"<a href=\"AddCities\">Back</a>");
        }else
            out.print("<p>Deleted the city"+cityResponse.toString()+"</p>"+"<a href=\"AddCities\">Back</a>");
    }
    protected void doPost(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException
    {
        List<Cities> cities= cityService.findCities();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (cities==null)
        {
            response.sendRedirect("/AddCity.jsp");
        }else
        {for (Cities city:cities
                 ) {
                out.print("<p>Name:"+city.getName()+" , latitude:"+city.getLatitude()+" ,longitude"+city.getLongitude()+" ,id= "+city.getId()+"</p><br>");
            }
            out.print("<a href=\"AddCities\">Back</a>");}
    }
}
