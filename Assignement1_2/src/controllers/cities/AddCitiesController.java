package controllers.cities;

import dto.CityDTO;
import entities.Cities;
import service.CityService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

@WebServlet("/AddCities")
public class AddCitiesController extends HttpServlet {

    private CityService cityService =new CityService();

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException
    {
        List<Cities> cities= cityService.findCities();
        List<CityDTO> modelCities=new ArrayList<>();
        for (Cities city: cities) {
            modelCities.add(new CityDTO(city.getId(),city.getLatitude(),city.getLongitude(),city.getName()));
        }
        request.setAttribute("cities", modelCities);
        request.getRequestDispatcher("/AddCity.jsp").forward(request, response);
    }
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException
    {
        CityDTO city=new CityDTO();
        String name= request.getParameter("nameCity");
        double latitude= Double.parseDouble(request.getParameter("latitude"));
        double longitude= Double.parseDouble(request.getParameter("longitude"));
        city.name=name;
        city.latitude=latitude;
        city.longitude=longitude;
        Cities cityResponse= cityService.addCities(city);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (cityResponse.getId()<0)
        {
            out.print("<p>Could not save city!</p><br>"+"<a href=\"AddCities\">Back</a>");
        }else
            out.print("<p>Saved the city"+cityResponse.toString()+"</p>"+"<a href=\"AddCities\">Back</a>");
    }

}
