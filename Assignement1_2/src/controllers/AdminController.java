package controllers;

import dto.CityDTO;
import dto.FlightDTO;
import entities.Cities;
import entities.Flights;
import service.CityService;
import service.FlightService;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.*;

public class AdminController extends HttpServlet {
    private CityService cityService =new CityService();
    private FlightService flightService =new FlightService();
    protected void doPost(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException
    {
        List<Flights> flights= flightService.findFlights();
        List<FlightDTO> modelFlights=new ArrayList<>();
        for (Flights flight: flights) {
            modelFlights.add(new FlightDTO(flight.getId(),flight.getFlightNumber(),flight.getAirplaneType(),flight.getCitiesByDepartureCityId().getName(),flight.getDepartureCityDatetime().toString(),
                    flight.getCitiesByArrivalCityId().getName(),flight.getArrivalCityDatetime().toString()));
        }
        List<Cities> cities= cityService.findCities();
        List<CityDTO> citiesModel=new ArrayList<>();
        for (Cities city: cities) {
            citiesModel.add(new CityDTO(city.getId(),city.getLatitude(),city.getLongitude(),city.getName()));
        }
        request.setAttribute("cities", citiesModel);
        request.setAttribute("flights", modelFlights);
        request.getRequestDispatcher("/AddFlights.jsp").forward(request, response);
    }
}
