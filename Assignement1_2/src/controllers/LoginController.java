package controllers;

import entities.User;
import service.UsersService;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class LoginController extends HttpServlet {
    private UsersService usersService =new UsersService();
    public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException,ServletException
    {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        User user= usersService.findByUsernameAndPassword(username,password);
        if(user==null)
        {
            out.print("Sorry username or password error!");
            RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
            rd.include(request,response);
        }
        if(user.getRole().equals("ADMIN"))
        {
            RequestDispatcher rd=request.getRequestDispatcher("Admin");
             rd.forward(request,response);
        }
        else {
            RequestDispatcher rd=request.getRequestDispatcher("Client");
            rd.forward(request,response);
        }
        out.close();
    }
}
