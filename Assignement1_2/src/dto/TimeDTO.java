package dto;

public class TimeDTO {
    public int dstOffset;
    public int rawOffset;
    public String status;
    public String timeZoneId;
    public String timeZoneName;
}
