package dto;

public class CityDTO {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int id;
    public Double latitude;
    public Double longitude;
    public String name;

    public CityDTO(){

    }

    public CityDTO(int id, double lat, double lng, String name){
        this.id=id;
        this.latitude=lat;
        this.longitude=lng;
        this.name=name;
    }
    @Override
    public String toString(){
        return  "{ \"id\":"+id+", \"name\":\""+name+"\", \"latitude\":"+latitude+", \"longitude\":"+longitude+" }";
    }
}
