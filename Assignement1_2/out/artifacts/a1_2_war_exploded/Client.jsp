<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Hello {client name here}!</title>
</head>
<body>
<div>
    List of Flights:<br>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Flight Number</th>
            <th>Airplane type</th>
            <th>Departure City</th>
            <th>Departure Time</th>
            <th>Arrival City</th>
            <th>Arrival Time</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="element" items="${flights}" >
            <tr>
                <td>${element.id}</td>
                <td>${element.flightNumber}</td>
                <td>${element.airplaneType}</td>
                <td>${element.departureCity}</td>
                <td>${element.departureTime}</td>
                <td>${element.arrivalCity}</td>
                <td>${element.arrivalTime}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<form action="SearchFlight" method="post">
    Search a flight by cities:<br>
    Departure city:
    <select name="departureCity2">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <br>
    Arrival city:
    <select name="arrivalCity2">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <br>
    <input type="submit" value="Search Flight">
</form>
<form action="FindTime" method="post">
    Search a local time(select flight):<br>
    <select name="flightNumberSearchTime">
        <c:forEach var="element" items="${flights}">
            <option value="${element.flightNumber}">${element.flightNumber}
            </option>
        </c:forEach>
    </select>
    <select name="citySearchTime">
        <option value="departure">Departure</option>
        <option value="arrival">Arrival</option>
    </select>
    <input type="submit" value="Search time">
</form>
</body>
</html>
