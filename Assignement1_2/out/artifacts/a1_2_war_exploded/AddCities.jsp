<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Add new city</title>
</head>
<body>
<form action="AddCities" method="post">
    Please input the city data the click submit to add:<br>
    Name:<input type="text" name="nameCity" size="20px">
    Latitude:<input type="text" name="latitude" size="20px">
    Longitude:<input type="text" name="longitude" size="20px">
    <input type="submit" value="submit">
</form>
<form action="DeleteCities" method="get">
    Delete a city, input name:<br>
    Name:<input type="text" name="nameCityDelete" size="20px">
    <input type="submit" value="submit">
</form>
<form action="DeleteCities" method="post">
    Click the button to view cities:
    <input type="submit" value="submit">
</form>

<div>
    List of Cities:<br>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Latitude</th>
            <th>Longitude</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="element" items="${cities}" >
            <tr>
                <td>${element.id}</td>
                <td>${element.name}</td>
                <td>${element.latitude}</td>
                <td>${element.longitude}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<a href="AddFlights">Back</a>
</body>
</html>
