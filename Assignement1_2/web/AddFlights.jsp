<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html >
<head>
    <title>Add a flight</title>
</head>
<body>
<form action="AddFlights" method="post">
    Please enter data, then click submit to add flight: <br>
    Flight number:<input type="text" name="flightNumber"size="20px" pattern="[0-9]" title="Insert a number!" required><br>
    Airplane type:<input type="text" name="airplaneType"size="20px" required><br>
    Departure date and time(yyyy-mm-dd hh:mm:ss):<input type="datetime" pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}" title="Insert correct format!" name="departureDate"size="20px" required>
    Departure city:
    <select name="departureCity">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <br>
    Arrival date and time:<input type="datetime" pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}" title="Insert correct format!" name="arrivalDate"size="20px" required>
    Arrival city:
    <select name="arrivalCity">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <br>
    <input type="submit" value="Add Flight">
</form>
<br>
<br>
<form action="DeleteFlights" method="get">
    Delete a flight, input flight number:<br>
    Name:<input type="text" name="flightNumberDelete" pattern="[0-9]" title="Insert a number!" size="20px">
    <input type="submit" value="Delete Flight">
</form>
<form action="DeleteFlights" method="post">
    Search a flight by cities:<br>
    Departure city:
    <select name="departureCity2">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <br>
    Arrival city:
    <select name="arrivalCity2">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <br>
    <input type="submit" value="Search Flight">
</form>
<form action="UpdateFlights" method="post">
    Update a flight by selecting the flight number:<br>
    Flight number:
    <select name="updateFlightNumber">
        <c:forEach var="element" items="${flights}">
            <option value="${element.flightNumber}">${element.flightNumber}</option>
        </c:forEach>
    </select>
    Select field to update:
    <select name="updateFlightColumn">
            <option value="AirplaneType">Airplane type</option>
            <option value="DepartureCity">Departure city</option>
            <option value="DepartureTime">Departure time</option>
            <option value="ArrivalCity">Arrival city</option>
           <option value="ArrivalTime">Arrival time</option>
    </select>
    Airplane type:<input type="text" name="updateAirplaneType"size="20px"><br>
    Departure date and time:<input type="datetime" pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}" title="Insert correct format!" name="updateDepartureDate"size="20px"><br>
    Departure city:
    <select name="updateDepartureCity">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <br>
    Arrival date and time:<input type="datetime" pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}" title="Insert correct format!" name="updateArrivalDate"size="20px"><br>
    Arrival city:
    <select name="updateArrivalCity">
        <c:forEach var="element" items="${cities}">
            <option value="${element.id}">${element.name}</option>
        </c:forEach>
    </select>
    <input type="submit" value="Update Flight">
</form>
<br>
<div>
    List of Flights:<br>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Flight Number</th>
            <th>Airplane type</th>
            <th>Departure City</th>
            <th>Departure Time</th>
            <th>Arrival City</th>
            <th>Arrival Time</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="element" items="${flights}" >
            <tr>
                <td>${element.id}</td>
                <td>${element.flightNumber}</td>
                <td>${element.airplaneType}</td>
                <td>${element.departureCity}</td>
                <td>${element.departureTime}</td>
                <td>${element.arrivalCity}</td>
                <td>${element.arrivalTime}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<a href="AddCities">Add Cities</a>
</body>
</html>

