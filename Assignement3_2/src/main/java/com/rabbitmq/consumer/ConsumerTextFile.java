package com.rabbitmq.consumer;

import com.rabbitmq.Dvd;
import com.rabbitmq.client.*;
import org.springframework.amqp.utils.SerializationUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class ConsumerTextFile {
    private static final String EXCHANGE_NAME = "logs";
    public static void writeToFile(String filename,String message){
        PrintWriter writer;
        try {
            writer = new PrintWriter(filename+".txt", "UTF-8");
            writer.println(message);
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                Dvd message =   (Dvd) SerializationUtils.deserialize(body);
                writeToFile(message.getName()+message.getYear(),message.toString());
                System.out.println(" [x] Received in text file consumer with files'" + message.toString() + "'");
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }

}
