package com.rabbitmq.consumer;

import com.rabbitmq.Dvd;
import com.rabbitmq.client.*;
import com.rabbitmq.service.MailService;
import org.springframework.amqp.utils.SerializationUtils;

import java.io.IOException;

public class ConsumerEmail {
    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        MailService mailService = new MailService("manbogdan1996@gmail.com","11111111");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                Dvd message =   (Dvd) SerializationUtils.deserialize(body);
                mailService.sendMail("manbgdan1996@gmail.com","New Dvd added",message.toString());
                System.out.println(" [x] Received in email consumer with emails'" + message.toString() + "'");
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }

}
