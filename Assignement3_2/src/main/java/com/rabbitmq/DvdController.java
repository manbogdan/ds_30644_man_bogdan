package com.rabbitmq;

import com.rabbitmq.producer.ProducerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DvdController {
    @GetMapping("/dvd")
    public String dvdForm(Model model) {
        model.addAttribute("dvd", new Dvd());
        return "dvd";
    }

    @PostMapping("/dvd")
    public String dvdSubmit(@ModelAttribute Dvd dvd) {
        ProducerService emmiter=new ProducerService();
        System.out.println(dvd.toString());
        try{
        emmiter.sendMessage(dvd);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "result";
    }
}
