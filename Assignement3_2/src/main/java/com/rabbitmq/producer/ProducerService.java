package com.rabbitmq.producer;

import com.rabbitmq.Dvd;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.amqp.utils.SerializationUtils;

import java.util.concurrent.TimeoutException;

public class ProducerService {
    private static final String EXCHANGE_NAME = "logs";

    public void sendMessage(Dvd dvd)
            throws java.io.IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        String message = dvd.toString();
        byte[] data = SerializationUtils.serialize(dvd);
        channel.basicPublish(EXCHANGE_NAME, "", null, data);
        System.out.println(" [x] Sent '" + message + "'");

        channel.close();
        connection.close();
    }
}
